import bpy

# Création d'un nouvel opérateur pour créer un objet "Case"
class OBJECT_OT_CreateCase(bpy.types.Operator):
    bl_idname = "object.create_case"  # Identifiant de l'opérateur
    bl_label = "Create Case"  # Nom affiché dans l'interface utilisateur
    bl_options = {'REGISTER', 'UNDO'}  # Options de l'opérateur

    # Propriétés pour spécifier les dimensions de la case
    width: bpy.props.FloatProperty(name="Width", default=2, min=0.1)
    height: bpy.props.FloatProperty(name="Height", default=2, min=0.1)
    depth: bpy.props.FloatProperty(name="Depth", default=2, min=0.1)

    # Méthode exécutée lors de l'exécution de l'opérateur
    def execute(self, context):
        # Création d'un cube avec les dimensions spécifiées
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, align='WORLD', location=(0, 0, 0))
        bpy.context.active_object.scale = (self.width, self.height, self.depth)  # Redimensionnement de l'objet

        # Ajout d'un modificateur Bevel
        bpy.ops.object.modifier_add(type='BEVEL')
        bpy.context.object.modifiers["Bevel"].width = context.scene.bevel_width

        return {'FINISHED'}  # Indique que l'opération est terminée avec succès

# Panneau pour afficher une liste de contrôle
class VIEW3D_PT_Checklist_Panel(bpy.types.Panel):
    bl_space_type = "VIEW_3D"  # Type d'espace (ici, 3D Viewport)
    bl_region_type = "UI"  # Type de région
    bl_label = "My Checklist"  # Nom du panneau
    bl_category = "The only check-list you'll ever need"  # Catégorie dans laquelle le panneau est affiché

    # Méthode pour dessiner le contenu du panneau
    def draw(self, context):
        layout = self.layout

        # Ajout des cases à cocher pour différents éléments de la liste de contrôle
        layout.prop(context.scene, "is_folder_clean", text="Fichier est bien rangé dans le dossier")
        layout.prop(context.scene, "is_object_centered", text="Objet placé au centre du monde")
        layout.prop(context.scene, "is_object_scaled", text="Objet modélisé à la bonne échelle")
        layout.prop(context.scene, "is_pivot_centered", text="Centre de pivot bien placé")
        layout.prop(context.scene, "is_outliner_tidy", text="Outliner rangé, meshs parentés et bien renommés")
        layout.prop(context.scene, "are_stars_aligned", text="Etoiles bien placées")
        
        # Chloe s'est occupée de "MyCheckList", et a cherché comment mettre des cases à cocher avec Elodie.

# Panneau pour afficher une liste automatique d'opérations
class VIEW3D_PT_AutoList_Panel(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "My Auto-list"
    bl_category = "The only check-list you'll ever need"

    def draw(self, context):
        layout = self.layout
        
        # Ajout des opérations automatiques à la liste
        layout.prop(context.scene, "is_normals_flip", text="Les normales sont bien orientées")
        row = layout.row()
        row.operator("mesh.flip_normals", text="Flip Normals", icon="ORIENTATION_NORMAL")
        self.layout.separator()
        row.operator("mesh.normals_make_consistent", text="Recalculate Outside", icon="NORMALS_FACE")
        
        layout.prop(context.scene, "is_transforms_apply", text="Toutes les transformations sont appliquées")
        row = layout.row()
        row.operator("object.transform_apply", text="Apply All Transforms", icon="ERROR")
        
        self.layout.separator()
        layout.prop(context.scene, "is_bevel_apply", text="Le BEVEL !!!")
        row = layout.row()
        row.operator("object.bevel", text="Bevel moi ça", icon="MOD_BEVEL")
        
        self.layout.separator()
        layout.prop(context.scene, "is_shade_smooth_apply", text="Le shade smooth est appliqué")
        row = layout.row()
        row.operator("object.shade_smooth", text="Smooth operator", icon="SHADING_RENDERED")

        self.layout.separator()
        layout.prop(context.scene, "is_modifier_apply", text="Apply modifiers sauf Subdivision et Mirror")
        row = layout.row()
        row.operator("object.apply_modifiers_except_subsurf", text="Apply Modifiers", icon="MODIFIER")
        
        self.layout.separator()
        layout.prop(context.scene, "is_uv_unwrap", text="Les UV sont dépliés")
        row = self.layout.row()
        row.operator("uv.unwrap", text="UV Unwrap", icon="UV")
        self.layout.separator()
        row.operator("uv.unwrap", text="Smart UV project", icon="UV_DATA")
        
        # Elodie s'est occupée de "MyAutoList", et a recherché comment régler le problème de l'application de modifiers
        # et du bevel avec Chloe.

# Opérateur pour appliquer tous les modificateurs sauf Subdivision Surface
class OBJECT_OT_ApplyModifiersExceptSubsurf(bpy.types.Operator):
    bl_idname = "object.apply_modifiers_except_subsurf"
    bl_label = "Apply Modifiers Except Subdivision Surface"
    
    def execute(self, context):
        obj = context.active_object
        for mod in obj.modifiers:
            if mod.type not in {'SUBSURF', 'MIRROR'}:
                bpy.ops.object.modifier_apply(modifier=mod.name)
        return {'FINISHED'}

# Opérateur pour ajouter un modificateur Bevel
class OBJECT_OT_Bevel(bpy.types.Operator):
    bl_idname = "object.bevel"
    bl_label = "Add Bevel Modifier"
    
    def execute(self, context):
        obj = context.active_object
        bevel_modifier = obj.modifiers.new(name="Bevel", type='BEVEL')
        bevel_modifier.width = context.scene.bevel_width
        return {'FINISHED'}
    
    # On a recherché ensemble comment faire des classes personnalisés pour que les modifiers puissent s'appliqué sans le Subdiv et le mirror, et 
    # comment intégrer un bouton qui puisse mettre le modifier bevel 

def register():
    # Ajout de propriétés booléennes à la scène pour la liste de contrôle
    bpy.types.Scene.is_folder_clean = bpy.props.BoolProperty(name="Le fichier est bien rangé dans le dossier", default=False)
    bpy.types.Scene.is_object_centered = bpy.props.BoolProperty(name="Objet placé au centre du monde", default=False)
    bpy.types.Scene.is_object_scaled = bpy.props.BoolProperty(name="Objet modélisé à la bonne échelle", default=False)
    bpy.types.Scene.is_pivot_centered = bpy.props.BoolProperty(name="Centre de pivot bien placé", default=False)
    bpy.types.Scene.is_outliner_tidy = bpy.props.BoolProperty(name="Outliner rangé, meshs parentés et bien renommés", default=False)
    bpy.types.Scene.are_stars_aligned = bpy.props.BoolProperty(name="Etoiles bien placées", default=False)
    

    bpy.types.Scene.is_transforms_apply = bpy.props.BoolProperty(name="Toutes les transformations sont appliquées", default=False)
    bpy.types.Scene.is_normals_flip = bpy.props.BoolProperty(name="Les normales sont bien orientées", default=False)
    bpy.types.Scene.is_uv_unwrap = bpy.props.BoolProperty(name="Les UV sont dépliés", default=False)
    bpy.types.Scene.is_shade_smooth_apply = bpy.props.BoolProperty(name="Le shade smooth est appliqué", default=False)
    bpy.types.Scene.is_bevel_apply = bpy.props.BoolProperty(name="Le BEVEL !!!", default=False)
    bpy.types.Scene.is_modifier_apply = bpy.props.BoolProperty(name="Apply modifiers sauf Subdivision et Mirror", default=False)
    bpy.types.Scene.bevel_width = bpy.props.FloatProperty(name="Bevel Width", default=0.1, min=0)
    
    # Enregistrement des classes personnalisées dans Blender
    bpy.utils.register_class(VIEW3D_PT_Checklist_Panel)
    bpy.utils.register_class(VIEW3D_PT_AutoList_Panel)
    bpy.utils.register_class(OBJECT_OT_CreateCase)
    bpy.utils.register_class(OBJECT_OT_Bevel)
    bpy.utils.register_class(OBJECT_OT_ApplyModifiersExceptSubsurf)
    
def unregister():
    
    # Suppression des propriétés ajoutées à la scène
    bpy.utils.unregister_class(VIEW3D_PT_Checklist_Panel)
    bpy.utils.unregister_class(VIEW3D_PT_AutoList_Panel)
    bpy.utils.unregister_class(OBJECT_OT_CreateCase)
    bpy.utils.unregister_class(OBJECT_OT_Bevel)
    bpy.utils.unregister_class(OBJECT_OT_ApplyModifiersExceptSubsurf)
    del bpy.types.Scene.is_folder_clean
    del bpy.types.Scene.is_object_centered
    del bpy.types.Scene.is_object_scaled
    del bpy.types.Scene.is_pivot_centered
    del bpy.types.Scene.is_outliner_tidy
    del bpy.types.Scene.are_stars_aligned
    
    
    del bpy.types.Scene.is_transforms_apply
    del bpy.types.Scene.is_normals_flip
    del bpy.types.Scene.is_uv_unwrap
    del bpy.types.Scene.is_shade_smooth_apply
    del bpy.types.Scene.is_bevel_apply
    del bpy.types.Scene.is_modifier_apply
    del bpy.types.Scene.bevel_width
    
    #On aurait voulu faire en sorte que les cases de "MyAutoList" se cochent automatiquement quand l'action est faite, mais à cause d'un manque de temps (et du projet unity...)
    # on a pas du réussir à le faire. Cependant, on avait un début de réponse avec la partie du bevel, mais lorsqu'on supprimait le modifier, la case restait coché, on a pas pu aller jusqu'au bout malheuresement.
    # Mais avec un peu plus de temps, on pense qu'on aurait pu trouver la solution ! 
    
    #On aurait aussi voulu mettre le son "Smooth Operator" lorsqu'on shade smooth un objet, mais pareil, on a pas eu le temps de le faire...c'est un grand regrès...
    # "No need to ask, He's a smooth operator, Smoooooooooth operatooooor, Smooth operator, Smoooooooooth operatooooor" :emojimusic: ;)))

    register()